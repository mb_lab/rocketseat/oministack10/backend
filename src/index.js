const express = require('express');
const mongoose = require('mongoose');
const routes = require('./routes');

const app = express()

mongoose.connect('string de conexao');

app.use(express.json());
app.use(routes);

app.listen(3333);